# Discord Bots
Discord bots using [discord.py](https://github.com/Rapptz/discord.py)

## General Usage
Pass your token as an argument or through stdin.

### matchmaker.py
Puts users into a queue and matches them together. Does not support multiple servers.
