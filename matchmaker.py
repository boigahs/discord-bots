import discord
import sys
import logging
import datetime
import asyncio
import random

client = discord.Client()
waiters = {}
to = datetime.timedelta(minutes=1)

def prefname(user):
	return user.nick if user.nick else user.name

def dewait(user):
	waiters[user][1].cancel()
	del waiters[user]

async def waittimer(msg):
	await asyncio.sleep(10)
	while datetime.datetime.utcnow()-waiters[msg.author][0] < datetime.timedelta(minutes=10):
		opponents = list(waiters.keys())
		opponents.remove(msg.author)
		if opponents:
			op = random.choice(opponents)
			dewait(op)
			del waiters[msg.author]
			await msg.channel.send(f'{msg.author.mention} vs {op.mention}', delete_after=600)
			return
		await asyncio.sleep(1)
	await msg.channel.send(f'{prefname(msg.author)}: Matchmaking timed out', delete_after=600)
	return

@client.event
async def on_ready():
	print('Logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
	if message.author.bot:
		return 
	if message.content.startswith('.h'):
		await message.channel.send('\n.mm: join or leave matchmaking queue\n.wl: show waiting queue\n.t: show time in waiting queue\n.source: link to source code', delete_after=600)
		return
	if message.content.startswith('.source'):
		await message.channel.send('https://bitbucket.org/boigahs/discord-bots', delete_after=600)
		return
	if message.content.startswith('.mm'):
		if message.author not in waiters:
			t = asyncio.ensure_future(waittimer(message))
			waiters[message.author] = message.created_at, t
			await message.add_reaction(u'\u2705') # Green/white check mark
		else:
			dewait(message.author)
			await message.add_reaction(u'\u2705') # Green/white check mark
		return
	if message.content.startswith('.wl'):
		if not waiters:
			await message.add_reaction(u'\u274C') # Red cross mark
			return
		names = set()
		for w in waiters:
			names.add(prefname(w))
		out = "Currently waiting: "
		for n in names:
			out += n + ', '
		await message.channel.send(out[:-2], delete_after=600)
		return
	if message.content.startswith('.t'):
		if message.author in waiters:
			await message.channel.send(datetime.datetime.utcnow() - waiters[message.author][0], delete_after=600)
		else:
			await message.add_reaction(u'\u274C') # Red cross mark
		return

if len(sys.argv) == 2:
	token = sys.argv[1]
else:
	token = sys.stdin.read()[:-1] # Trim newline
client.run(token)
